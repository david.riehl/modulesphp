<?php
namespace ModulesPHP\modules\home;

use ModulesPHP\core\Template;

class Controller
{
    public static function homeAction() {
        $args = func_get_args();
        Template::addParameter("args", $args);
        Template::display();
    }
}