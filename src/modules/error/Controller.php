<?php
namespace ModulesPHP\modules\error;

use ModulesPHP\core\Template;

class Controller
{
    public static function pageNotFoundAction() {
        Template::addParameter("error_title", "404");
        Template::addParameter("error_message", "Page not found.");
        Template::display();
    }
}