<?php
namespace ModulesPHP\core;

class Routes
{
    private static $routes = null;
    private static $module_routes = [];

    private static function setRoutes() {
        $json = file_get_contents("src/routes.json");
        $routes = json_decode($json);
        foreach($routes as $route) {
            if (! property_exists($route, "action")) {
                $route->action = null;
            }
            self::$routes[] = new Route($route->path, $route->module, $route->action);
        }
    }

    public static function getRoutes() {
        if (self::$routes == null) { self::setRoutes(); }
        return self::$routes;
    }

    private static function setModuleRoutes($module) {
        $json = file_get_contents("src/modules/${module}/routes.json");
        $routes = json_decode($json);
        foreach($routes as $route) {
            self::$module_routes[$module][] = new Route($route->path, $route->module, $route->action);
        }
    }

    public static function getModuleRoutes($module) {
        if (!array_key_exists($module, self::$module_routes)) { self::setModuleRoutes($module); }
        return self::$module_routes[$module];
    }

}