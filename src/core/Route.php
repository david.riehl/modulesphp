<?php
namespace ModulesPHP\core;

class Route
{
    private $path;
    private $module;
    private $action;

    public function getPath() { return $this->path; }
    public function getModule() { return $this->module; }
    public function getAction() { return $this->action; }

    public function __construct($path, $module, $action) {
        $this->path = $path;
        $this->module = $module;
        $this->action = $action;
    }
}