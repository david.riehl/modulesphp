# Project Contributions

## How to Contribute

## Branches Policy

## Framework Architecture

```
root
|-- doc : documentation folder
|-- src : source code folder
|   |-- core : Framework Core Classes
|   |   |-- Dispatcher.php : Routing Management Class
|   |   |-- Route.php : Route Model Class
|   |   |-- Routers.php : Route Factory Class that load routes from routes.json files
|   |   |-- Template.php : Template Manager Class
|   |   `-- Uri.php : Uri Model Class
|   |-- model : Model folder for entities definitions
|   |-- modules : Modules folder for modules definitions
|   |   |-- error : Error module for Error Pages Management
|   |   |   |-- templates : templates folder
|   |   |   |   `-- index.php : main template
|   |   |   `-- Controller.php : Module Controller Class
|   |   `-- home : Home module for Home Page
|   |       |-- templates : templates folder
|   |       |   `-- index.php : main template
|   |       |-- Controller.php : Module Controller Class
|   |       `-- routes.json : Module Routes Definitions
|   |-- templates : Base Templates
|   |   `-- index.php : main base template
|   |-- index.php : dispatching
|   `-- routes.php : Main Routes Definitions
|-- test : tests folder
|-- vendor : dependencies
|-- .gitignore
|-- .htaccess : for URL Routing stuff
|-- composer.json : for dependencies management
|-- index.php : application entry point (autoload, redirection)
`-- README.md : Application presentation
```